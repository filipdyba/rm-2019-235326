import numpy as np
from numpy import linalg as la
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random
import math

#Wyznaczanie sciezki robota na podstawie kierunku wypadkowej sily potencjalow

#Zdefiniowanie dokladnosci skali
delta = 0.1
x_size = 10
y_size = 10

#Definicja parametrow metody
kp = 1
koi= 1
d0 = 40

#Wylosowanie polozenia poczatku, konca i przeszkod
obstacle_number=10
ko=[]
obst_vect = []
for i in range(obstacle_number):
    obst_vect.append((random.uniform(-10,10),random.uniform(-10,10)))
    ko.append(koi)
start_point=(-10,random.uniform(-10,10))
finish_point=(10,random.uniform(-10,10))

#Inicjalizacja zmiennych
Fo=0
d=0
Fp=0

#Zdefiniowanie osi ukladu wspolrzednych
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

#Obliczenie wartosci sil w kazdym punkcie
for i in xrange(len(x)):
    for k in xrange(len(y)):
        #Sila przyciagajaca do punktu koncowego
        Fp = kp*la.norm((x[i]-finish_point[0],y[k]-finish_point[1]))
        #Sily odpychajace od przeszkod
        for j in xrange(len(obst_vect)):
            d = la.norm((x[i]-obst_vect[j][0],y[k]-obst_vect[j][1]))
            if d <= d0 and d!=0: #Zabezpieczenie przed dzieleniem przez 0
                #Wartosc sily
                Fo-=ko[j]*(1/d-1/d0)*(1/d)*(1/d)
        #Suma sil w danym punkcie z uwzglednieniem zwrotu
        Z[k][i] = Fp-Fo
        #Znormalizowanie wartosci, aby wykres byl czytelny
        #wg skali dla sily przyciagajacej
        if Z[k][i] > kp*la.norm((finish_point[0]-start_point[0],
                                 finish_point[1]-start_point[1])):
            Z[k][i] = kp*la.norm((finish_point[0]-start_point[0],
                                  finish_point[1]-start_point[1]))
        #Wyzerowanie wartosci sil
        Fp=0
        Fo=0

#Wyznczanie sciezki
path = []
index_y = 0
index_x = 0
RPi = [0 for j in range(len(obst_vect))]#Wektor laczacy robota z i-ta przezkoda
RK = 0 #Wektor laczacy robota z meta
Fw = 0 #Sila wypadkowa
alfa = 0 #Kat sily wypadkowej wzgledem osi x
safety_variable = 0 #Zabezpiecznie przed wpadnieciem w minimum lokalne
#Odnalezienie punktu poczatkowego
while math.fabs(y[index_y]-start_point[1]) > delta:
    index_y+=1
#Poszukiwaie sciezki dopoki robot nie bedzie dostatecznie blisko
while la.norm((x[index_x]-finish_point[0], y[index_y]-finish_point[1])) > 2*delta and safety_variable<500:
    path.append((x[index_x],y[index_y]))
    #Sila przyciagajaca do mety
    #Wspolrzedne wektora sily przyciagajacej robota do mety(uwzglednia kierunek)
    #(sila_potencjalu)*(wektor_RK)/(norma_wektora_RK)
    RK = kp*np.array([finish_point[0]-x[index_x],finish_point[1]-y[index_y]])
    #Sily odpychajace od przeszkod
    for j in xrange(len(obst_vect)):
        d = la.norm((obst_vect[j][0]-x[index_x],obst_vect[j][1]-y[index_y]))
        if d <= d0 and d!=0: #Zabezpieczenie przed dzieleniem przez 0
            #Wspolrzedne wektora sily potencjalu laczacy robota z przeszkoda
            #(wartosc_sily_potencjalu)*(wektor_RPi)/(norma_RPi)
            RPi[j] = ko[j]*(1/d-1/d0)*(1/d**3)*np.array([obst_vect[j][0]-x[index_x],obst_vect[j][1]-y[index_y]])
        else: #jesli robot nie widzi przeszkody
            RPi[j]=0
    #Obliczenie sily wypadkowej z uwzglednieniem zwrtou
    Fw = RK
    for i in range(len(RPi)):
        Fw -= RPi[i]

    #Wyznaczenie kierunku dzialania Fw
    alfa = np.arctan2(Fw[1],Fw[0])

    #Wybor kolejnego punktu na mapie
    if alfa>-np.pi/8 and alfa<=np.pi/8:
        index_x+=1
    elif alfa>np.pi/8 and alfa<=3*np.pi/8:
        index_x+=1
        index_y+=1
    elif alfa>3*np.pi/8 and alfa<=5*np.pi/8:
        index_y+=1
    elif alfa>5*np.pi/8 and alfa<=7*np.pi/8:
        index_x-=1
        index_y+=1
    elif alfa>7*np.pi/8 or alfa<=-7*np.pi/8:
        index_x-=1
    elif alfa<=-5*np.pi/8 and alfa>-7*np.pi/8:
        index_x-=1
        index_y-=1
    elif alfa>-5*np.pi/8 and alfa<=-3*np.pi/8:
        index_y-=1
    elif alfa>-3*np.pi/8 and alfa<=-np.pi/8:
        index_x+=1
        index_y-=1
    if index_x==len(x) or index_y==len(y):
        break
    safety_variable+=1


    
#Rysowanie wykresu
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

for path_element in path:
    plt.plot(path_element[0], path_element[1], "*", color='white')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
