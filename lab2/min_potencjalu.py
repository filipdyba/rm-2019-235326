import numpy as np
from numpy import linalg as la
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random
import math

#Wyznaczanie sciezki robota na podstawie wartosci potencjalu w danym punkcie
#Robot zawsze wybiera minimalna wartosc potencjalu ze swojego otoczenia lokalnego wg ustalonej kolejnosci

#Zdefiniowanie dokladnosci skali
delta = 0.1
x_size = 10
y_size = 10

#Definicja parametrow metody
kp = 1
koi= 1
d0 = 40

#Wylosowanie polozenia poczatku, konca i przeszkod
obstacle_number=8
ko=[]
obst_vect = []
for i in range(obstacle_number):
    obst_vect.append((random.uniform(-10,10),random.uniform(-10,10)))
    ko.append(koi)
start_point=(-10,random.uniform(-10,10))
finish_point=(10,random.uniform(-10,10))

#Inicjalizacja zmiennych
Fo=0
d=0
Fp=0

#Zdefiniowanie osi ukladu wspolrzednych
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

#Obliczenie wartosci sil w kazdym punkcie
for i in xrange(len(x)):
    for k in xrange(len(y)):
        #Sila przyciagajaca do punktu koncowego
        Fp = kp*la.norm((x[i]-finish_point[0],y[k]-finish_point[1]))
        #Sily odpychajace od przeszkody
        for j in xrange(len(obst_vect)):
            d = la.norm((x[i]-obst_vect[j][0],y[k]-obst_vect[j][1]))
            if d <= d0 and d!=0: #Zabezpieczenie przed dzieleniem przez 0
                #Wartosc
                Fo-=ko[j]*(1/d-1/d0)*(1/d)*(1/d)
        Z[k][i] = Fp-Fo
        #Znormalizowanie wartosci, aby wykres byl czytelny
        #Skala wedlug sily przyciagajacej
        if Z[k][i] > kp*la.norm((finish_point[0]-start_point[0],
                                 finish_point[1]-start_point[1])):
            Z[k][i] = kp*la.norm((finish_point[0]-start_point[0],
                                  finish_point[1]-start_point[1]))
        #Wyzerowanie wartosci sil
        Fp=0
        Fo=0

#Wyznczanie sciezki
path = []
index_y = 0
index_x = 0
safety_variable = 0 #Zabezpiecznie przed wpadnieciem w minimum lokalne
#Odnalezienie punktu poczatkowego
while math.fabs(y[index_y]-start_point[1]) > delta:
    index_y+=1
#Poszukiwaie sciezki dopoki robot nie bedzie dostatecznie blisko
while la.norm((x[index_x]-finish_point[0], y[index_y]-finish_point[1])) > 2*delta and safety_variable<500:
    path.append((x[index_x], y[index_y]))
    #Zapewnienie, ze robot nie wyjedzie poza znany obszar
    if index_y == 0:
        bottom=0
    else:
        bottom=1
    if index_y == len(y)-1:
        top=0
    else:
        top=1
    if index_x==0:
        left=0
    else:
        left=1
    if index_x==len(x)-1:
        right=0
    else:
        right=1

    #Odnalezienie kolejnego etapu sciezki - punktu o najmniejszym potencjale z lokalnego otoczenia robota
    elements = [Z[index_y+top][index_x], Z[index_y+top][index_x+right],
            Z[index_y][index_x+right], Z[index_y-bottom][index_x+right],
            Z[index_y-bottom][index_x],Z[index_y-bottom][index_x-left],
            Z[index_y][index_x-left], Z[index_y+top][index_x-left]]
    chosen_index = elements.index(min(elements))
    del elements
    #Aktualizacja wspolrzednych punktu
    if chosen_index == 0:
        index_y += top
    elif chosen_index == 1:
        index_y += top
        index_x += right
    elif chosen_index == 1:
        index_x += right
    elif chosen_index == 3:
        index_y -= bottom
        index_x += right
    elif chosen_index == 2:
        index_y -= bottom
    elif chosen_index == 5:
        index_y -= bottom
        index_x -= left
    elif chosen_index == 3:
        index_x -= left
    elif chosen_index == 7:
        index_y += top
        index_x -= left
    safety_variable += 1

#Rysowanie wykresu
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

for path_element in path:
    plt.plot(path_element[0], path_element[1], "*", color='blue')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
