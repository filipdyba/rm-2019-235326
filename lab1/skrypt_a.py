import numpy as np
from numpy import linalg as la
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random
import math

#Zdefiniowanie dokladnosci skali
delta = 0.01
x_size = 10
y_size = 10

#Wylosowanie polozenia poczatku, konca i przeszkod
obst_vect = [(random.uniform(-10,10),random.uniform(-10,10)),
             (random.uniform(-10,10),random.uniform(-10,10)),
             (random.uniform(-10,10),random.uniform(-10,10)),
             (random.uniform(-10,10),random.uniform(-10,10))]
start_point=(-10,random.uniform(-10,10))
finish_point=(10,random.uniform(-10,10))

#Definicja parametrow metody
kp = 1
koi = 1
ko = []
for i in range(len(obst_vect)):
    ko.append(koi);
d0 = 40

#Inicjalizacja zmiennych
Fo=0
d=0
Fp=0

#Zdefiniowanie osi ukladu wspolrzednych
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

#Obliczenie wartosci sil w kazdym punkcie
for i in xrange(len(x)):
    for k in xrange(len(y)):
        #Sila przyciagajaca do punktu koncowego
        Fp = kp*la.norm((x[i]-finish_point[0],y[k]-finish_point[1])) 
        for j in xrange(len(obst_vect)):
            d = la.norm((x[i]-obst_vect[j][0],y[k]-obst_vect[j][1]))
            if d <= d0 and d!=0: #Zabezpieczenie przed dzieleniem przez 0
                #Sila odpychajaca od przeszkody
                Fo-=ko[j]*(1/d-1/d0)*(1/d)*(1/d)
        #Suma sil w danym punkcie z uwzglednieniem zwrotu
        Z[k][i] = Fp-Fo
        #Znormalizowanie wartosci, aby wykres byl czytelny
        #Normalizacja zalezna od kp i polozenia poczatku i konca
        if Z[k][i] > kp*la.norm((finish_point[0]-start_point[0],
                                finish_point[1]-start_point[1])):
            Z[k][i] = kp*la.norm((finish_point[0]-start_point[0],
                                 finish_point[1]-start_point[1]))
        #Wyzerowanie wartosci sil dla dalszych obliczen
        Fp=0
        Fo=0

#Rysowanie wykresu
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
